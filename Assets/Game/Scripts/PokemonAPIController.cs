﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokemonAPIController : MonoBehaviour
{
    [Serializable]
    public class PokemonInformation
    {
        public int id;
        public string name;
        public PokemonSprite sprites;
    }

    [Serializable]
    public class PokemonSprite
    {
        public string front_default;
    }


    public InputField indexInput;
    public Image pokemonImage;
    public Text pokemonNameText;
    public Button catchButton;

    public int totalCurrentPokemonCaught = 0;

    public PokemonInformation pokemonInformation;

    private string receivedPokemon;


    private void Start()
    {
        catchButton.interactable = false;
    }
    public void GetPokemon()
    {
        try
        {
            //Default index to use
            int index = 1;

            if (!string.IsNullOrWhiteSpace(indexInput.text))
            {
                index = Convert.ToInt32(indexInput.text);

                if (index <= 0)
                {
                    index = 1;
                }
            }

            string url = "https://pokeapi.co/api/v2/pokemon/" + index;

            HttpWebRequest request = HttpWebRequest.CreateHttp(url);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            StreamReader reader = new StreamReader(response.GetResponseStream());

            receivedPokemon = reader.ReadToEnd();

            reader.Close();
            reader.Dispose();

            pokemonInformation = ParsePokemonInfo();

            StartCoroutine(ConvertPokemonSprite());

            UpdateShownInfo();
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    private IEnumerator ConvertPokemonSprite()
    {

        UnityWebRequest request = UnityWebRequest.Get(pokemonInformation.sprites.front_default);
        yield return request.SendWebRequest();


        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            byte[] results = request.downloadHandler.data;

            Texture2D tex = new Texture2D(0,0);
            tex.LoadImage(results);


            Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero, 100.0f);

            pokemonImage.sprite = sprite;
            pokemonImage.enabled = true;

        }
    }

    private void UpdateShownInfo()
    {
        pokemonNameText.text = pokemonInformation.name;
        catchButton.interactable = true;

    }

    private PokemonInformation ParsePokemonInfo()
    {
        return JsonUtility.FromJson<PokemonInformation>(receivedPokemon);
    }
}
