﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    public Text totalCaughtText;
    public Text totalPlayerCaughtText;

    public Text confirmText;
    public Text currentPokemonName;

    public List<string> pokemonList;

    private int totalCaught = 0;
    private int totalCurrentPlayerCaught = 0;

    private bool firstPull = true;
    private void FixedUpdate()
    {
        if (firstPull == true && PlayfabManager.Instance.state == PlayfabManager.LoginState.Success && string.IsNullOrEmpty(PlayfabManager.Instance.displayName) == false)
        {
            firstPull = false;
            GetTotalPokemonCaught();
        }
    }

    public void PostCaughtPokemon()
    {
        confirmText.text = "";

    PlayFabClientAPI.UpdateUserData(
    new UpdateUserDataRequest
    {
        Data = new Dictionary<string, string>
        {
            {currentPokemonName.text + ", " + System.DateTime.Now, LocationServicesController.Instance.latitude.ToString() + ", " + LocationServicesController.Instance.longitude}
        },
        Permission = UserDataPermission.Public
    },
        result =>
        {
            confirmText.text = "Pokemon caught and uploaded to Playfab";
            UpdateTotalCaughtPokemon();
        },
        error => {
            confirmText.text = "Failed to upload to prefab";
        }
        );
    }

    public void UpdateTotalCaughtPokemon()
    {
        PlayFabClientAPI.UpdatePlayerStatistics(
            new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                        new StatisticUpdate { StatisticName = "Total_Pokemon_Caught", Value = (totalCurrentPlayerCaught += 1) }
                }
            },
            new System.Action<UpdatePlayerStatisticsResult>(UpdatePlayerStatisticsResponse),
            new System.Action<PlayFabError>(PlayerStatisicUpdateErrorCallback)
        );
    }

    public void PlayerStatisicUpdateErrorCallback(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
    }

    public void UpdatePlayerStatisticsResponse(UpdatePlayerStatisticsResult result)
    {
        confirmText.text = "Pokemon caught and uploaded to Playfab";
        GetTotalPokemonCaught();
    }

    public void GetTotalPokemonCaught()
    {
        PlayFabClientAPI.GetLeaderboard(
            new GetLeaderboardRequest
            {
                StatisticName = "Total_Pokemon_Caught",
                StartPosition = 0
            },
            new System.Action<GetLeaderboardResult>(PokemonLeaderboardResultCallback),
            new System.Action<PlayFabError>(PokemonLeaderboardErrorCallback)
        );
    }

    public void PokemonLeaderboardResultCallback(GetLeaderboardResult result)
    {
        totalCaught = 0;
        foreach (PlayerLeaderboardEntry entry in result.Leaderboard)
        {
            totalCaught += entry.StatValue;

            if (!string.IsNullOrEmpty(PlayfabManager.Instance.displayName))
            {
                if (entry.Profile.DisplayName == PlayfabManager.Instance.displayName)
                {
                    totalCurrentPlayerCaught = entry.StatValue;
                }
            }
        }

        totalCaughtText.text = "Total Pokemon Caught: " + totalCaught.ToString();
        totalPlayerCaughtText.text = "Total You Caught: " + totalCurrentPlayerCaught.ToString();
    }

    public void PokemonLeaderboardErrorCallback(PlayFabError error)
    {
        Debug.LogError("Leaderboard had an error:");
        confirmText.text = error.GenerateErrorReport();
    }

}
