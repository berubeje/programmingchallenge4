﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSettings : MonoBehaviour
{
    public InputField userNameText;
    public Button submitButton;
    public Text pokemonCanvasUsernameText;


    private bool waiting = true;


    private void Start()
    {
        submitButton.interactable = false;
        userNameText.interactable = false;

    }
    private void FixedUpdate()
    {
        if (waiting == true && PlayfabManager.Instance.usernameReceiveAttempted == true)
        {
            submitButton.interactable = true;
            userNameText.interactable = true;
            waiting = false;
        }
    }

    public void UpdateUserName()
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(
            new UpdateUserTitleDisplayNameRequest() { DisplayName = userNameText.text },
            result =>
            {
                Debug.Log("Success");
                pokemonCanvasUsernameText.text = result.DisplayName;
                GameManager.Instance.ChangeToPokemonCanvas();
            },
            error =>
            {
                Debug.Log("Failed to update username");
            }
        );
    }
}
