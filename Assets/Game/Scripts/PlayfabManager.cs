﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayfabManager : Singleton<PlayfabManager>
{
    public enum LoginState
    {
        Startup,
        Instantiated,
        Success,
        Failed,
    }
    private string playerGUID;
    public LoginState state = LoginState.Startup;
    public bool createNewPlayer;

    public bool usernameReceiveAttempted = false;

    public GameObject usernameCanvas;
    public GameObject pokemonCanvas;

    public Text userNameText;


    public string displayName = "";

    private void Awake()
    {
        playerGUID = SystemInfo.deviceUniqueIdentifier;
    }

    private void Start()
    {
        state = LoginState.Instantiated;

        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest { CustomId = playerGUID, CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        state = LoginState.Success;


        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest(),
            requestResult =>
            {
                if(requestResult.PlayerProfile.DisplayName == null)
                {
                    GameManager.Instance.ChangeToUsernameCanvas();
                }
                else
                {
                    displayName = requestResult.PlayerProfile.DisplayName;
                    userNameText.text = displayName;

                    GameManager.Instance.ChangeToPokemonCanvas();

                }
                usernameReceiveAttempted = true;
            },
            error =>
            {
                Debug.Log(error.ErrorMessage);
            }
        );
    }

    private void OnLoginFailure(PlayFabError error)
    {
        state = LoginState.Failed;
        Debug.LogWarning("Something went wront logging into PlayFab :(");
        Debug.LogError(error.GenerateErrorReport());
    }

}
