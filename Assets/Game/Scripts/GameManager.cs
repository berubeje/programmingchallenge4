﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public GameObject usernameCanvas;
    public GameObject pokemonCanvas;



    public void ChangeToUsernameCanvas()
    {
        if (usernameCanvas != null && pokemonCanvas != null)
        {
            usernameCanvas.SetActive(true);
            pokemonCanvas.SetActive(false);
        }
    }

    public void ChangeToPokemonCanvas()
    {
        if (usernameCanvas != null && pokemonCanvas != null)
        {
            usernameCanvas.SetActive(false);
            pokemonCanvas.SetActive(true);
        }
    }
}
